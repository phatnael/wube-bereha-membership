const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const passport = require("passport");
const LocalStrategy = require("passport-local");
const methodOverride = require("method-override");
/*const fs = require("fs")
const path = require("path")*/
const app = express();
require("dotenv/config");

const User = require("./models/user");

const PORT = process.env["PORT"] || 8080;

// MONGOOSE CONFIG
mongoose.connect(process.env["MONGO_URL"], {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
mongoose.set("useCreateIndex", true);
mongoose.Promise = global.Promise;

// SERVER CONFIGS
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan(":method :url :status"));
app.set("view engine", "pug");
app.use(methodOverride("_method"));
// app.set("view engine", "ejs")

// PASSPORT CONFIG
app.use(
  require("express-session")({
    secret: "wube bereha membership webapp",
    resave: false,
    saveUninitialized: false,
  })
);

app.use(passport.initialize());
app.use(passport.session());
passport.use("local", new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

/* ROUTES */
// MEMBER ROUTES
app.use("/members", require("./routes/member"));
// PRODUCT ROUTES
app.use("/products", require("./routes/product"));
// TIER ROUTES
app.use("/tiers", require("./routes/tier"));
// INVOICE ROUTES
app.use("/invoices", require("./routes/invoice"));
// USER ROUTES
app.use("/users", require("./routes/user"));

// ERROR HANDLING MIDDLEWARE
app.use((err, req, res, next) => {
  console.log(err);
  res.status(421).send({ error: err.message });
});

// SERVER
app.listen(PORT, () => {
  console.log(`server listening on port ${PORT}`);
});
