const { Client } = require('whatsapp-web.js');
const QRCode = require("qrcode")
const client = new Client();

client.on('qr', (qr) => {
    // Generate and scan this code with your phone
    QRCode.toFile('whatsapp.png', `${qr}`, {color: {dark: "#000", light: "#0000"}},function (err) {
        if(err) throw err
        console.log('generated qr')
    })
    console.log('QR RECEIVED', qr);
});

client.on('ready', () => {
    console.log('Client is ready!');
});

client.on('message', msg => {
    if (msg.body === '!ping') {
        msg.reply('pong');
    }
});

client.initialize();