const QRCode = require("qrcode");

QRCode.toFile(
  "modeth-white.jpg",
  "https://www.moderneth.com",
  {
    color: {
      dark: "#000",
      light: "#fff",
    },
  },
  function (err) {
    if (err) throw err;
    console.log("done");
  }
);
