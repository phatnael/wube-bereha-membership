const mongoose = require("mongoose");
const Member = require("./member");

const TierSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  amount: {
    type: Number,
    required: true,
  },
});

const TierModel = mongoose.model("Tier", TierSchema);
module.exports = TierModel;
