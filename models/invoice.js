const mongoose = require("mongoose");
const Member = require("./member");
const Product = require("./product");

const InvoiceSchema = new mongoose.Schema({
  products: [
    {
      product:{
        type: mongoose.Schema.ObjectId,
        ref: "Product",
      },
      qty: {
        type: Number
      }
    },
  ],
  member: {
    type: mongoose.Schema.ObjectId,
    ref: "Member",
  },
  created_on: {
    type: Date,
    default: Date.now(),
  },
});

// charges members for the product they purchased
InvoiceSchema.methods.charge = async function (memberId, productId) {
  let member = await Member.findById(memberId)
    .then((m) => {
      return m;
    })
    .catch((err) => {
      console.log(err);
    });
  let balance = member.balance;
  let product = await Product.findById(productId)
    .then((p) => {
      return p;
    })
    .catch((err) => {
      console.log(err);
    });
  balance -= product.cost;

  if (balance >= 0) {
    Member.findByIdAndUpdate(memberId, { balance: balance })
      .then((m) => {
        console.log(m);
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

// check if the products changed
InvoiceSchema.methods.checkProductChange = function (prod) {
  if (prod.length === this.products.length) {
    let counter = 0;
    prod.forEach((p) => {
      if (this.products.includes(p)) {
        counter++;
      }
    });
    return counter !== this.products.length;
  } else {
    return true;
  }
};

// check if the members changed
InvoiceSchema.methods.checkMemberChange = function (member) {
  // return this.members === members;
  return this.member.toString() !== member;
};

InvoiceSchema.methods.getProductsTotal = function(){
  let total = 0
  for(let i=0;i<this.products.length;i++){
    total += this.products[i].cost
  }
  return total
}

const InvoiceModel = mongoose.model("invoice", InvoiceSchema);
module.exports = InvoiceModel;
