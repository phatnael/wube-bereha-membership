const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  cost: {
    type: Number,
    required: true,
  },
});

const ProductModel = mongoose.model("Product", ProductSchema);
module.exports = ProductModel;
