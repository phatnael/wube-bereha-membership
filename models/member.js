const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Tier = require("./tier");
const Invoice = require("./invoice");

const MemberSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: true,
  },
  last_name: {
    type: String,
    required: true,
  },
  phone_number: {
    type: String,
    required: true,
  },
  balance: {
    type: Number,
    // required: true,
  },
  membership: {
    type: mongoose.Schema.ObjectId,
    ref: "Tier",
  },
  password: {
    type: String,
  },
  /*  qr_code: {
    data: Buffer,
    contentType: String,
  },*/
});

MemberSchema.pre("save", function (next) {
  let member = this;

  // only hash the password if it has been modified or is new
  if (!member.isModified("password")) return next();

  // generate salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    // hash our password using our new salt
    bcrypt.hash(member.password, salt, function (err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      member.password = hash;
      next();
    });
  });
});

MemberSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

MemberSchema.methods.changeMembership = async function (tier_id) {
  let tier = await Tier.findById(tier_id)
    .then((t) => {
      return t;
    })
    .catch((err) => {
      return err;
    });
  if (tier) {
    let tier_amount = tier.amount;
    this.balance = tier_amount - this.balance;
    this.membership = tier_id;
    this.save();
    return this;
  } else {
    return "tier does not exist";
  }
};

const MemberModel = mongoose.model("Member", MemberSchema);
module.exports = MemberModel;
