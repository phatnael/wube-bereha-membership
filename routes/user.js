const router = require("express").Router();
const User = require("../models/user");
const passport = require("passport");

// REGISTER USER
router.post("/register", (req, res, next) => {
  let newUser = newUser({
    username: req.body.username,
  });
  if (req.body.adminCode === process.env.ADMIN_CODE) {
    newUser.isAdmin = true;
  }
  User.register(newUser, req.body.password, function (err, user) {
    if (err) {
      res.json({
        err: err,
      });
    }
    passport.authenticate("local")(req, res, function () {
      res.json({
        msg: "success",
      });
    });
  });
});

// LOGOUT USER
router.post(
  "/login",
  passport.authenticate("local", {
    successRedirect: "/members",
    failureRedirect: "/users",
  }),
  function (req, res) {}
);

// LOGOUT USER
router.get("/logout", (req, res, next) => {
  req.logout();
  res.redirect("/users");
});

module.exports = router;
