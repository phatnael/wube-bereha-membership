const router = require("express").Router();
const Product = require("../models/product");
const Invoice = require("../models/invoice");
const member = require("../models/member");

// INDEX PRODUCTS
router.get("/", (req, res, next) => {
  Product.find({})
    .then((products) => {
      res.render("product/index", { products: products });
    })
    .catch((err) => {
      res.json(next);
    });
});

// CREATE VIEW
router.get("/create", async (req, res, next) => {
  res.render("product/create");
});

// CREATE PRODUCT
router.post("/", (req, res, next) => {
  const product = new Product({
    name: req.body.name,
    cost: req.body.cost,
  });

  Product.create(product)
    .then((product) => {
      res.redirect("/products");
    })
    .catch((err) => {
      res.json({
        next,
      });
    });
});

// FIND A PRODUCT
router.get("/:id", async (req, res, next) => {
  let invoices = await Invoice.find({ products: req.params.id })
    .populate("member")
    .then((i) => {
      return i;
    })
    .catch(next);
  Product.findById(req.params.id)
    .then((product) => {
      res.render("product/details", { product: product, invoices: invoices });
    })
    .catch(next);
});

// EDIT VIEW
router.get("/:id/edit", async (req, res, next) => {
  let product = await Product.findById(req.params.id)
    .then((p) => {
      return p;
    })
    .catch(next);
  res.render("product/edit", { product: product });
});

// EDIT A PRODUCT
router.put("/:id", (req, res, next) => {
  let product = {
    name: req.body.name,
    cost: req.body.cost,
  };
  Product.findByIdAndUpdate(req.params.id, product)
    .then((product) => {
      res.redirect("/products");
    })
    .catch(next);
});

// DELETE A PRODUCT
router.delete("/:id", (req, res, next) => {
  Product.findOneAndRemove({ _id: req.params.id })
    .then((product) => {
      res.json({
        product: product,
      });
    })
    .catch(next);
});

module.exports = router;
