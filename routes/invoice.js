const bodyParser = require("body-parser");

const router = require("express").Router();
const Invoice = require("../models/invoice");
const Member = require("../models/member");
const Product = require("../models/product");

const accountSID = "ACab756571a520d45ace181b312e40cb77";
const authToken = "f804d585622ea72e88934aa8d847030c";
const client = require("twilio")(accountSID, authToken);

// INDEX INVOICES
router.get("/", async (req, res, next) => {
  let foundInvoices = [];
  let invoices = await Invoice.find({})
    .populate("member")
    .populate("products")
    .then((invoices) => {
      return invoices;
    })
    .catch(next);
  console.log(invoices);

  for (let i = 0; i < invoices.length; i++) {
    foundInvoices.push({
      invoice: invoices[i],
      total: invoices[i].getProductsTotal(),
    });
  }
  console.log(foundInvoices);
  res.render("invoice/index", { invoices: foundInvoices });
});
// RENDER CREATE VIEW
router.get("/create", async (req, res, next) => {
  let members = await Member.find({})
    .then((m) => {
      return m;
    })
    .catch(next);
  let products = await Product.find({})
    .then((p) => {
      return p;
    })
    .catch(next);
  res.render("invoice/create", {
    members: members,
    products: products,
  });
});
// VIEW INVOICE
router.get("/:id", (req, res, next) => {
  Invoice.findById(req.params.id)
    .populate("member")
    .populate("products.product")
    .then((invoice) => {
      res.render("invoice/details", {
        invoice: invoice,
        total: invoice.getProductsTotal(),
      });
    })
    .catch(next);
});

// CREATE INVOICE
router.post("/", bodyParser.json(), async (req, res, next) => {
  let invoice = new Invoice({
    products: req.body.products,
    member: req.body.member,
  });

  let total_charge = 0;

  for (let i = 0; i < invoice.products.length; i++) {
    let product = await Product.findById(invoice.products[i].product)
      .then((p) => {
        return p;
      })
      .catch((err) => {
        console.log(err);
      });
    total_charge += product.cost;
    await invoice.charge(invoice.member, invoice.products[i].product);
  }
  let member = await Member.findById(invoice.member)
    .then((m) => {
      return m;
    })
    .catch(next);

  if (member.balance > total_charge) {
    Invoice.create(invoice)
      .then((invoice) => {
        res.json(invoice);
      })
      .catch(next);
  } else {
    res.json("insufficient balance");
  }
});

// DELETE INVOICE
router.delete("/:id", (req, res, next) => {
  Invoice.findByIdAndRemove(req.params.id)
    .then((invoice) => {
      res.json({
        invoice: invoice,
      });
    })
    .catch(next);
});

// RENDER EDOT VIEW
router.get("/:id/edit", async (req, res, next) => {
  let members = await Member.find({})
    .then((m) => {
      return m;
    })
    .catch(next);

  let products = await Product.find({})
    .then((p) => {
      return p;
    })
    .catch(next);

  await Invoice.findById(req.params.id)
    .populate("member")
    .populate("products.product")
    .then((i) => {
      res.render("invoice/edit", {
        invoice: i,
        members: members,
        products: products,
      });
    })
    .catch(next);
});

// EDIT INVOICE
router.put("/:id", async (req, res, next) => {
  let invoice = {
    products: req.body.products,
    member: req.body.member,
  };
  let inv = await Invoice.findById(req.params.id)
    .then((i) => {
      return i;
    })
    .catch(next);

  let savedMember = await Member.findById(inv.member)
    .then((m) => {
      return m;
    })
    .catch(next);

  /*  res.json({
    productChange: inv.checkProductChange(invoice.products),
    memberChange: inv.checkMemberChange(invoice.members),
  });*/

  // if the products have changed, refund or charge extra accordingly
  if (inv.checkProductChange(invoice.products)) {
    // current products cost & saved amount
    let currentCost = 0,
      savedCost = 0;
    for (let i = 0; i < invoice.products.length; i++) {
      let prod = await Product.findById(invoice.products[i])
        .then((pro) => {
          return pro;
        })
        .catch(next);
      currentCost += prod.cost;
    }
    for (let i = 0; i < inv.products.length; i++) {
      let prod = await Product.findById(inv.products[i])
        .then((p) => {
          return p;
        })
        .catch(next);
      savedCost += prod.cost;
    }
    let difference = savedCost - currentCost;
    let balance = savedMember.balance;
    let newBalance = balance + difference;
    savedMember.balance = newBalance;
    savedMember.save();
    inv.products = invoice.products;
    inv.save();

    res.json({
      currentCost: currentCost,
      savedCost: savedCost,
      difference: difference,
      savedBalance: balance,
      newBalance: newBalance,
    });
  }
  // if the members has changed update members, charge the cost to the new members and refund the money to the previously saved members
  if (inv.checkMemberChange(invoice.member)) {
    let cost = 0;
    for (let i = 0; i < invoice.products.length; i++) {
      let product = await Product.findById(invoice.products[i])
        .then((p) => {
          return p;
        })
        .catch(next);
      cost += product.cost;
    }
    // saved members
    let savedMember = await Member.findById(inv.member)
      .then((m) => {
        return m;
      })
      .catch(next);
    // received members
    let newMember = await Member.findById(invoice.member)
      .then((m) => {
        return m;
      })
      .catch(next);
    // refund money to previous members
    savedMember.balance += cost;
    savedMember.save();

    // change members & charge received members
    inv.member = invoice.member;
    newMember.balance -= cost;
    newMember.save();
    inv.save();
    if (!inv.checkProductChange()) {
      res.json({
        invoice: inv,
        previousMember: savedMember._id,
        newMember: newMember._id,
      });
    }
  }
  res.json({
    msg: "nothing to change",
  });
});

// SEND INVOICE WHATSAPP MESSAGE
router.get("/:id/send", async (req, res, next) => {
  /*let numberOfTabs = function (items, item) {
    let tabs = "";
    let longest = "";
    for (let i = 0; i < items.length; i++) {
      if (items[i].length > longest) {
        longest = items[i];
      }
    }
    let len = longest.length - item.length;
    console.log(len)
/!*    for (let i = 0; i < len; i++) {
      // tabs += "\t";
      // console.log(len)
    }*!/
    // console.log(tabs.length);
    return tabs;
  };*/
  let invoice = await Invoice.findById(req.params.id)
    .populate("products")
    .then((inv) => {
      return inv;
    })
    .catch(next);
  // console.log(invoice);
  if (invoice) {
    // message body
    let msgBody = "You have purchased\n\n";
    let totalCost = 0;
    for (let i = 0; i < invoice.products.length; i++) {
      totalCost += invoice.products[i].cost;
      /*      msgBody +=
        invoice.products[i].name +
        "" +
        invoice.products[i].cost +
        " Birr \n";*/
      msgBody += `${
        invoice.products[i].name +
        numberOfTabs(invoice.products, invoice.products[i])
      }${invoice.products[i].cost} Birr\n`;
    }
    let d = invoice.created_on;
    let y = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
    let m = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
    let day = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
    let h = new Intl.DateTimeFormat("en", { hour: "2-digit" }).format(d);
    let min = new Intl.DateTimeFormat("en", { minute: "2-digit" }).format(d);
    let invDate = `${day}-${m}-${y}\t\t\t${h}:${min}`;
    msgBody += "\nDate\t\t\t\t\t\t\t\t\t\t" + invDate + "\n";
    msgBody += "\nTotal\t\t\t\t\t\t\t\t\t\t" + totalCost + " Birr";
    // send whatsapp message
    client.messages
      .create({
        from: "whatsapp:+14155238886",
        body: msgBody,
        to: "whatsapp:+251914283259",
      })
      .then((message) => res.json({ body: message.body }))
      .catch((err) => res.json({ err: err.message }));
  } else {
    res.json({
      err: "invoice doesn't exist.",
    });
  }
});

module.exports = router;
