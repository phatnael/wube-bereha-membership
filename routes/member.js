const router = require("express").Router();
const Member = require("../models/member");
const Tier = require("../models/tier");
const Invoice = require("../models/invoice");
const QRCode = require("qrcode");

// INDEX MEMBERS
router.get("/", async (req, res, next) => {
  function getInvoices(mid) {
    return Invoice.find({ member: mid }, function (err, invoices) {
      if (err) {
        return err;
      } else {
        return invoices;
      }
    });
  }
  let foundInvoices = [];
  Member.find({})
    .populate("membership")
    .then((members) => {
      for (let i = 0; i < members.length; i++) {
        foundInvoices.push(getInvoices(members[i]));
      }
      res.render("member/index", { members: members, invoices: foundInvoices });
    })
    .catch(next);
});

// RENDER CREATE VIEW
router.get("/create", async (req, res, next) => {
  let tiers = await Tier.find({})
    .then((t) => {
      return t;
    })
    .catch(next);
  res.render("member/create", { tiers: tiers });
});

// CREATE A MEMBER
router.post("/", (req, res, next) => {
  let member = new Member({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    phone_number: req.body.phone_number,
    membership: req.body.membership,
    password: req.body.password,
  });

  Tier.findById(req.body.membership)
    .then((tier) => {
      member.balance = tier.amount;
    })
    .catch(next);

  Member.create(member)
    .then((member) => {
      res.redirect("/members");
    })
    .catch(next);
});

// VIEW MEMBER
router.get("/:id", async (req, res, next) => {
  const member = await Member.findById(req.params.id)
    .populate("membership")
    .then((member) => {
      return member;
    })
    .catch(next);
  const invoices = await Invoice.find({ member: member._id })
    .populate("products.product")
    .then((i) => {
      return i;
    })
    .catch(next);
  let foundInVoices = [];
  for (let i = 0; i < invoices.length; i++) {
    foundInVoices.push({
      invoice: invoices[i],
      total: invoices[i].getProductsTotal(),
    });
  }
  console.log(foundInVoices);
  res.render("member/details", { member: member, invoices: foundInVoices });
});

// RENDER EDIT PAGE
router.get("/:id/edit", async (req, res, next) => {
  let tiers = await Tier.find({})
    .then((t) => {
      return t;
    })
    .catch(next);
  let member = await Member.findById(req.params.id)
    .then((m) => {
      return m;
    })
    .catch(next);
  console.log(tiers);
  res.render("member/edit", { member: member, tiers: tiers });
});

// EDIT A MEMBER
router.put("/:id", (req, res, next) => {
  let member = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    phone_number: req.body.phone_number,
  };

  Member.findByIdAndUpdate(req.params.id, member).then((member) => {
    res.redirect(`/members/${member._id}`);
  });
});

// DELETE A MEMBER
router.delete("/:id", (req, res, next) => {
  Member.findByIdAndDelete(req.params.id)
    .then((member) => {
      res.json({
        member: member,
      });
    })
    .catch(next);
});

// VIEW MEMBER INVOICES
router.get("/:id/invoices", (req, res, next) => {
  Invoice.find({ member: req.params.id })
    .then((invoices) => {
      res.json({
        count: invoices.length,
        invoices: invoices,
      });
    })
    .catch(next);
});

// GENERATE QR
router.get("/:id/generate_qr", async (req, res, next) => {
  let member = await Member.findById(req.params.id)
    .then((m) => {
      return m;
    })
    .catch(next);

  if (member) {
    QRCode.toFile(
      `qr_codes/${member._id}_${member.first_name}_${member.last_name}.png`,
      `${member._id}`,
      {
        color: {
          dark: "#000",
          light: "#0000",
        },
      },
      function (err) {
        if (err) res.json({ err: err });
        res.json({ msg: "generated qr code" });
      }
    );
  } else {
    res.json({
      msg: "error",
    });
  }
});

// UPDATE MEMBERSHIP
router.put("/:id/upgrade/:mid", async (req, res, next) => {
  // tier to upgrade to
  let tier = await Tier.findById(req.params.mid)
    .then((t) => {
      return t;
    })
    .catch(next);
  // members to upgrade
  let member = await Member.findById(req.params.id)
    .populate("membership")
    .then((m) => {
      return m;
    })
    .catch(next);
  let tier_amount = tier.amount;
  let balance = member.balance;
  let fee = tier_amount - balance;
  member.balance += fee;
  member.membership = tier._id;
  member.save();
  res.json({
    fee: fee,
    member: member,
  });
  /*  let members = await Member.findById(req.params.id)
    .populate("membership")
    .then((m) => {
      return m;
    })
    .catch(next);
  res.json({
    members: members.changeMembership(req.params.mid),
  });*/
});

module.exports = router;
