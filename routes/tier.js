const router = require("express").Router();
const Tier = require("../models/tier");
const Member = require("../models/member");

// INDEX TIERS
router.get("/", async (req, res, next) => {
  let foundTiers = [];
/*  let foundMembers = async function (tid) {
    return await Member.find({ membership: tid }).count()
      .then((t) => {
        return t;
      })
      .catch(next);
  };*/
    let foundMembers = function(tid){
        return Member.find({membership: tid}, function (err, members) {
            if(err){
                return err
            }else{
                return members
            }
        })
    }
  await Tier.find({})
    .then((tiers) => {
      for (let i = 0; i < tiers.length; i++) {
        foundTiers.push({
          tier: tiers[i],
          members: foundMembers(tiers[i]._id),
        });
      }
    })
    .catch((err) => {
      res.json(next);
    });
  console.log(foundTiers);
  res.render("tier/index", { tiers: foundTiers });
});

// CREATE A TIER
router.post("/", (req, res, next) => {
  const tier = new Tier({
    name: req.body.name,
    amount: req.body.amount,
  });

  Tier.create(tier)
    .then((tier) => {
      res.json({ tier: tier });
    })
    .catch((err) => {
      res.json({
        next,
      });
    });
});

// VIEW TIER MEMBERS
router.get("/:id/members", (req, res, next) => {
  Member.find({ membership: req.params.id })
    .then((members) => {
      res.json({
        members: members,
      });
    })
    .catch(next);
});

// FIND A TIER
router.get("/:id", (req, res, next) => {
  Tier.findById(req.params.id)
    .then((tier) => {
      res.json({ tier: tier });
    })
    .catch(next);
});

// EDIT A TIER
router.put("/:id", (req, res, next) => {
  let tier = {
    name: req.body.name,
    amount: req.body.amount,
  };
  Tier.findByIdAndUpdate(req.params.id, tier)
    .then((tier) => {
      res.json({
        tier: tier,
      });
    })
    .catch(next);
});

// DELETE A TIER
router.delete("/:id", (req, res, next) => {
  Tier.findOneAndRemove({ _id: req.params.id })
    .then((tier) => {
      res.json({
        tier: tier,
      });
    })
    .catch(next);
});

module.exports = router;
